<?php

namespace App\Http\Requests;

class AccountCreateRequest extends ApiRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|unique:loyalty_account|max:255',
            'phone' => 'required|unique:loyalty_account|max:255',
            'card'  => 'required|unique:loyalty_account|max:255',
            'email_notification' => 'numeric|min:0|max:1',
            'phone_notification' => 'numeric|min:0|max:1',
        ];
    }
}
