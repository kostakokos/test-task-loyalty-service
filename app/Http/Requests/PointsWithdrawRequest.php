<?php

namespace App\Http\Requests;

class PointsWithdrawRequest extends ApiRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'account_type'  => 'required',
            'account_id'    => 'required',
            'points_amount' => 'required|numeric|min:0',
            'description'   => 'required',
        ];
    }
}
