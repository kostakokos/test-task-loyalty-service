<?php

namespace App\Http\Requests;

class PointsDepositRequest extends ApiRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'account_type' => 'required',
            'account_id'   => 'required',
            'description'  => 'required',
            'payment_amount' => 'required',
        ];
    }
}
