<?php

namespace App\Http\Requests;

class UserRegistrationRequest extends ApiRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => 'required|unique:users|max:255',
            'name'     => 'required|max:255',
            'password' => 'required|max:255',
        ];
    }
}
