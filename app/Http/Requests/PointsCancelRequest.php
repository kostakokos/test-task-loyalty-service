<?php

namespace App\Http\Requests;

class PointsCancelRequest extends ApiRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'cancellation_reason' => 'required',
            'transaction_id'      => 'required',
        ];
    }

    public function messages(): array
    {
        return [
            'cancellation_reason.required' => 'Cancellation reason is not specified'
        ];
    }
}
