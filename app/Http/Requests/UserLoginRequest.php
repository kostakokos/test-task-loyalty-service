<?php

namespace App\Http\Requests;

class UserLoginRequest extends ApiRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => 'required',
            'password' => 'required',
        ];
    }
}
