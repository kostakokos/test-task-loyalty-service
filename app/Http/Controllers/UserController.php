<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrationRequest;
use App\Service\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param UserRegistrationRequest $request
     * @return JsonResponse
     */
    public function register(UserRegistrationRequest $request): JsonResponse
    {
        $user = $this->userService->create(
            $request->input('name'),
            $request->input('email'),
            $request->input('password')
        );
        $token = $this->userService->createApiToken($user);

        return response()->json(['user' => $user, 'token' => $token], 201);
    }

    /**
     * @param UserLoginRequest $request
     * @return JsonResponse
     */
    public function login(UserLoginRequest $request): JsonResponse
    {
        $userDataToken = $this->userService->login(
            $request->input('email'),
            $request->input('password')
        );

        if (!$userDataToken) {
            return response()->json(['message' => 'Bad credentials'], 401);
        }

        return response()->json($userDataToken, 201);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $this->userService->logout($request->user());

        return response()->json(['message' => 'Logged out']);
    }
}
