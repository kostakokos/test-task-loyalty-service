<?php

namespace App\Http\Controllers;

use App\Http\Requests\PointsCancelRequest;
use App\Http\Requests\PointsDepositRequest;
use App\Http\Requests\PointsWithdrawRequest;
use App\Repository\LoyaltyAccountRepository;
use App\Repository\LoyaltyPointsTransactionRepository;
use App\Service\LoyaltyPointsTransactionService;
use App\Service\Notification\PointsMailSender;
use App\Service\Notification\PointsSmsSender;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class LoyaltyPointsController extends Controller
{
    private LoyaltyAccountRepository $accountRepository;
    private LoyaltyPointsTransactionRepository $transactionRepository;
    private LoyaltyPointsTransactionService $transactionService;

    /**
     * @param LoyaltyAccountRepository $accountRepository
     * @param LoyaltyPointsTransactionRepository $pointsTransactionRepository
     * @param LoyaltyPointsTransactionService $transactionService
     */
    public function __construct(
        LoyaltyAccountRepository $accountRepository,
        LoyaltyPointsTransactionRepository $pointsTransactionRepository,
        LoyaltyPointsTransactionService $transactionService
    )
    {
        $this->accountRepository = $accountRepository;
        $this->transactionService = $transactionService;
        $this->transactionRepository = $pointsTransactionRepository;
    }

    /**
     * @param PointsDepositRequest $request
     * @return JsonResponse
     */
    public function deposit(PointsDepositRequest $request): JsonResponse
    {
        Log::info('Deposit transaction input: ' . print_r($request->all(), true));

        $account = $this->accountRepository->searchByType(
            $request->input('account_type'),
            $request->input('account_id')
        );

        if (!$account) {
            Log::info('Account is not found');
            return response()->json(['message' => 'Account is not found'], 400);
        }
        if (!$account->isActive()) {
            Log::info('Account is not active');
            return response()->json(['message' => 'Account is not active'], 400);
        }
        $transaction = $this->transactionService->performPaymentLoyaltyPoints(
            $account->id,
            $request->input('loyalty_points_rule'),
            $request->input('description'),
            $request->input('payment_id'),
            $request->input('payment_amount'),
            $request->input('payment_time')
        );
        Log::info($transaction);

        (new PointsMailSender($account, $transaction))->send();
        (new PointsSmsSender($account, $transaction))->send();

        return response()->json($transaction);
    }

    /**
     * @param PointsCancelRequest $request
     * @return JsonResponse
     */
    public function cancel(PointsCancelRequest $request): JsonResponse
    {
        $reason = $request->input('cancellation_reason');
        $transactionId = $request->input('transaction_id');

        if ($transaction = $this->transactionRepository->findByIdCanceled($transactionId, 0)) {
            $this->transactionService->setCancellationReason($transaction, $reason);
            return response()->json(['message' => 'Successfully']);
        }

        return response()->json(['message' => 'Transaction is not found'], 400);
    }

    /**
     * @param PointsWithdrawRequest $request
     * @return JsonResponse
     */
    public function withdraw(PointsWithdrawRequest $request): JsonResponse
    {
        Log::info('Withdraw loyalty points transaction input: ' . print_r($request->all(), true));

        $type         = $request->input('account_type');
        $id           = $request->input('account_id');
        $pointsAmount = $request->input('points_amount');
        $description  = $request->input('description');

        if (!$account = $this->accountRepository->searchByType($type, $id)) {
            Log::info('Account is not found');
            return response()->json(['message' => 'Account is not found'], 400);
        }

        if (!$account->isActive()) {
            Log::info('Account is not active');
            return response()->json(['message' => 'Account is not active'], 400);
        }

        if ($pointsAmount <= 0) {
            Log::info('Wrong loyalty points amount: ' . $pointsAmount);
            return response()->json(['message' => 'Wrong loyalty points amount'], 400);
        }

        $balance = $this->transactionRepository->getBalance($account);
        if ($balance < $pointsAmount) {
            Log::info('Insufficient funds: ' . $pointsAmount);
            return response()->json(['message' => 'Insufficient funds'], 400);
        }

        $transaction = $this->transactionService->withdrawLoyaltyPoints($account, $pointsAmount, $description);

        Log::info($transaction);

        return response()->json($transaction);
    }
}
