<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccountCreateRequest;
use App\Repository\LoyaltyAccountRepository;
use App\Repository\LoyaltyPointsTransactionRepository;
use App\Service\LoyaltyAccountService;
use App\Service\Notification\ActivatedAccountMailSender;
use App\Service\Notification\ActivatedAccountSmsSender;
use App\Service\Notification\DeactivatedAccountMailSender;
use App\Service\Notification\DeactivatedAccountSmsSender;
use Illuminate\Http\JsonResponse;

class AccountController extends Controller
{
    private LoyaltyAccountRepository $accountRepository;
    private LoyaltyAccountService $accountService;

    /**
     * @param LoyaltyAccountRepository $accountRepository
     * @param LoyaltyAccountService $accountService
     */
    public function __construct(
        LoyaltyAccountRepository $accountRepository,
        LoyaltyAccountService $accountService
    )
    {
        $this->accountRepository = $accountRepository;
        $this->accountService    = $accountService;
    }

    /**
     * @param AccountCreateRequest $request
     * @return JsonResponse
     */
    public function create(AccountCreateRequest $request): JsonResponse
    {
        $account = $this->accountRepository->create($request->all());

        return response()->json($account, 201);
    }

    /**
     * @param $type
     * @param $id
     * @return JsonResponse
     */
    public function activate($type, $id): JsonResponse
    {
        if (!$account = $this->accountRepository->searchByType($type, $id)) {
            return response()->json(['message' => 'Account is not found'], 400);
        }

        if ($this->accountService->activate($account, true)) {
            (new ActivatedAccountMailSender($account))->send();
            (new ActivatedAccountSmsSender($account))->send();
        }

        return response()->json(['success' => true]);
    }

    /**
     * @param $type
     * @param $id
     * @return JsonResponse
     */
    public function deactivate($type, $id): JsonResponse
    {
        if (!$account = $this->accountRepository->searchByType($type, $id)) {
            return response()->json(['message' => 'Account is not found'], 400);
        }

        if ($this->accountService->activate($account, false)) {
            (new DeactivatedAccountMailSender($account))->send();
            (new DeactivatedAccountSmsSender($account))->send();
        }

        return response()->json(['success' => true]);
    }

    /**
     * @param LoyaltyPointsTransactionRepository $transactionRepository
     * @param $type
     * @param $id
     * @return JsonResponse
     */
    public function balance(LoyaltyPointsTransactionRepository $transactionRepository, $type, $id): JsonResponse
    {
        if ($account = $this->accountRepository->searchByType($type, $id)) {
            $balance = $transactionRepository->getBalance($account);

            return response()->json(['balance' => $balance], 400);
        }

        return response()->json(['message' => 'Account is not found'], 400);
    }
}
