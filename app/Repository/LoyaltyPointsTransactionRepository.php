<?php

namespace App\Repository;

use App\Models\LoyaltyAccount;
use App\Models\LoyaltyPointsTransaction;

class LoyaltyPointsTransactionRepository
{
    /**
     * @param $id
     * @param int|null $canceled
     * @return LoyaltyPointsTransaction|null
     */
    public function findByIdCanceled($id, ?int $canceled = null): ?LoyaltyPointsTransaction
    {
        $pointsTransactionQuery = LoyaltyPointsTransaction::where('id', '=', $id);

        if ($canceled !== null) {
            $pointsTransactionQuery->where('canceled', '=', 0);
        }

        return $pointsTransactionQuery->first();
    }

    /**
     * @param array $data
     * @return LoyaltyPointsTransaction
     */
    public function create(array $data): LoyaltyPointsTransaction
    {
        return LoyaltyPointsTransaction::create($data);
    }

    /**
     * @param LoyaltyAccount $account
     * @return float
     */
    public function getBalance(LoyaltyAccount $account): float
    {
        return LoyaltyPointsTransaction::where('canceled', '=', 0)
            ->where('account_id', '=', $account->id)->sum('points_amount');
    }
}
