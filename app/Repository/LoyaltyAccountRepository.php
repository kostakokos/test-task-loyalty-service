<?php

namespace App\Repository;

use App\Models\LoyaltyAccount;

class LoyaltyAccountRepository
{
    const TYPE_PHONE = 'phone';
    const TYPE_CARD  = 'card';
    const TYPE_EMAIL = 'email';

    /**
     * @param string $type
     * @param string $id
     * @return LoyaltyAccount|null
     */
    public function searchByType(string $type, string $id): ?LoyaltyAccount
    {
        if (in_array($type, [self::TYPE_PHONE, self::TYPE_CARD, self::TYPE_EMAIL])) {
            return LoyaltyAccount::where($type, '=', $id)->first();
        }

        throw new \InvalidArgumentException('Wrong parameters');
    }

    /**
     * @param array $date
     * @return LoyaltyAccount
     */
    public function create(array $date) : LoyaltyAccount
    {
        return LoyaltyAccount::create($date);
    }
}
