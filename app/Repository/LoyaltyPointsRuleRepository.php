<?php

namespace App\Repository;

use App\Models\LoyaltyPointsRule;

class LoyaltyPointsRuleRepository
{
    /**
     * @param $pointsRule
     * @return LoyaltyPointsRule|null
     */
    public function findPointsRule($pointsRule): ?LoyaltyPointsRule
    {
        return LoyaltyPointsRule::where('points_rule', '=', $pointsRule)->first();
    }
}
