<?php

namespace App\Service;

use App\Models\LoyaltyAccount;
use App\Models\LoyaltyPointsRule;
use App\Models\LoyaltyPointsTransaction;
use App\Repository\LoyaltyPointsRuleRepository;
use App\Repository\LoyaltyPointsTransactionRepository;

class LoyaltyPointsTransactionService
{
    /**
     * @param $accountId
     * @param $pointsRule
     * @param $description
     * @param $paymentId
     * @param $paymentAmount
     * @param $paymentTime
     * @return LoyaltyPointsTransaction
     */
    public function performPaymentLoyaltyPoints(
        $accountId,
        $pointsRule,
        $description,
        $paymentId,
        $paymentAmount,
        $paymentTime
    ): LoyaltyPointsTransaction
    {
        $pointsAmount = 0;

        $loyaltyPointsRule = new LoyaltyPointsRuleRepository();
        if ($pointsRule = $loyaltyPointsRule->findPointsRule($pointsRule)) {
            $pointsAmount = match ($pointsRule->accrual_type) {
                LoyaltyPointsRule::ACCRUAL_TYPE_RELATIVE_RATE => ($paymentAmount / 100) * $pointsRule->accrual_value,
                LoyaltyPointsRule::ACCRUAL_TYPE_ABSOLUTE_POINTS_AMOUNT => $pointsRule->accrual_value
            };
        }

        $pointsTransactionRepository = new LoyaltyPointsTransactionRepository();

        return $pointsTransactionRepository->create([
            'account_id' => $accountId,
            'points_rule' => $pointsRule?->id,
            'points_amount' => $pointsAmount,
            'description' => $description,
            'payment_id' => $paymentId,
            'payment_amount' => $paymentAmount,
            'payment_time' => $paymentTime,
        ]);
    }

    /**
     * @param LoyaltyPointsTransaction $transaction
     * @param string $reason
     * @return void
     */
    public function setCancellationReason(LoyaltyPointsTransaction $transaction, string $reason): void
    {
        $transaction->canceled = time();
        $transaction->cancellation_reason = $reason;
        $transaction->save();
    }

    /**
     * @param LoyaltyAccount $account
     * @param $pointsAmount
     * @param $description
     * @return LoyaltyPointsTransaction
     */
    public function withdrawLoyaltyPoints(LoyaltyAccount $account, $pointsAmount, $description): LoyaltyPointsTransaction
    {
        return (new LoyaltyPointsTransactionRepository())->create([
            'account_id' => $account->id,
            'points_rule' => 'withdraw',
            'points_amount' => -$pointsAmount,
            'description' => $description,
        ]);
    }
}
