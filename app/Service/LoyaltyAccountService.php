<?php

namespace App\Service;

use App\Models\LoyaltyAccount;

class LoyaltyAccountService
{
    /**
     * @param LoyaltyAccount $account
     * @param bool $param
     * @return bool
     */
    public function activate(LoyaltyAccount $account, bool $param): bool
    {
        if ($account->active != $param) {
            $account->active = $param;
            $account->save();

            return true;
        }

        return false;
    }
}
