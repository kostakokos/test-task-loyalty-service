<?php

namespace App\Service\Notification;

use App\Models\LoyaltyAccount;
use App\Models\LoyaltyPointsTransaction;
use App\Repository\LoyaltyPointsTransactionRepository;
use Illuminate\Support\Facades\Log;

class PointsSmsSender extends AbstractSender
{
    private LoyaltyPointsTransaction $transaction;

    /**
     * @param LoyaltyAccount $account
     * @param LoyaltyPointsTransaction $transaction
     */
    public function __construct(LoyaltyAccount $account, LoyaltyPointsTransaction $transaction)
    {
        parent::__construct($account);
        $this->transaction = $transaction;
    }

    /**
     * @return void
     */
    public function send(): void
    {
        if ($this->account->isSendSms()) {
            $balance = (new LoyaltyPointsTransactionRepository())->getBalance($this->account);
            Log::info('You received' . $this->transaction->getPointsAmount() . 'Your balance' . $balance);
        }

    }
}
