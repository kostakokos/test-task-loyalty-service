<?php

namespace App\Service\Notification;

use App\Mail\AccountDeactivated;
use Illuminate\Support\Facades\Mail;

class DeactivatedAccountMailSender extends AbstractSender
{
    /**
     * @return void
     */
    public function send(): void
    {
        if ($this->account->isSendMail() && !$this->account->isActive()) {
            Mail::to($this->account)->send(new AccountDeactivated());
        }
    }
}
