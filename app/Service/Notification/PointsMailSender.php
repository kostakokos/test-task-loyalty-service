<?php

namespace App\Service\Notification;

use App\Mail\LoyaltyPointsReceived;
use App\Models\LoyaltyAccount;
use App\Models\LoyaltyPointsTransaction;
use App\Repository\LoyaltyPointsTransactionRepository;
use Illuminate\Support\Facades\Mail;

class PointsMailSender extends AbstractSender
{
    private LoyaltyPointsTransaction $transaction;

    /**
     * @param LoyaltyAccount $account
     * @param LoyaltyPointsTransaction $transaction
     */
    public function __construct(LoyaltyAccount $account, LoyaltyPointsTransaction $transaction)
    {
        parent::__construct($account);
        $this->transaction = $transaction;
    }

    /**
     * @return void
     */
    public function send(): void
    {
        if ($this->account->isSendMail()) {
            $balance = (new LoyaltyPointsTransactionRepository())->getBalance($this->account);
            Mail::to($this->account)->send(new LoyaltyPointsReceived($this->transaction->getPointsAmount(), $balance));
        }

    }
}
