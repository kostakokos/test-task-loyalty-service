<?php

namespace App\Service\Notification;

use App\Mail\AccountActivated;
use App\Repository\LoyaltyPointsTransactionRepository;
use Illuminate\Support\Facades\Mail;

class ActivatedAccountMailSender extends AbstractSender
{
    public function send(): void
    {
        if ($this->account->isSendMail() && $this->account->isActive()) {
            $balance = (new LoyaltyPointsTransactionRepository())->getBalance($this->account);
            Mail::to($this->account)->send(new AccountActivated($balance));
        }
    }
}
