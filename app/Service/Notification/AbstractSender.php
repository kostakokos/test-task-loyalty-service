<?php

namespace App\Service\Notification;

use App\Models\LoyaltyAccount;

abstract class AbstractSender
{
    protected LoyaltyAccount $account;

    /**
     * @param LoyaltyAccount $account
     */
    public function __construct(LoyaltyAccount $account)
    {
        $this->account = $account;
    }

    abstract public function send(): void;
}
