<?php

namespace App\Service\Notification;

use Illuminate\Support\Facades\Log;

class ActivatedAccountSmsSender extends AbstractSender
{
    public function send(): void
    {
        if ($this->account->isSendSms() && $this->account->isActive()) {
            // instead SMS component
            Log::info('Account: phone: ' . $this->account->getPone() . ' ' . 'Activated');
        }
    }
}
