<?php

namespace App\Models;

use App\Repository\LoyaltyPointsRuleRepository;
use App\Repository\LoyaltyPointsTransactionRepository;
use Illuminate\Database\Eloquent\Model;

class LoyaltyPointsTransaction extends Model
{
    protected $table = 'loyalty_points_transaction';

    protected $fillable = [
        'account_id',
        'points_rule',
        'points_amount',
        'description',
        'payment_id',
        'payment_amount',
        'payment_time',
    ];

    /**
     * @return int
     */
    public function getPointsAmount(): int
    {
        return (int)$this->points_amount;
    }
}
