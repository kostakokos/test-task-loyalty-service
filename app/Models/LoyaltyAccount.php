<?php

namespace App\Models;

use App\Mail\AccountActivated;
use App\Mail\AccountDeactivated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class LoyaltyAccount extends Model
{
    protected $table = 'loyalty_account';

    protected $fillable = [
        'phone',
        'card',
        'email',
        'email_notification',
        'phone_notification',
        'active',
    ];

    /**
     * @return bool
     */
    public function isSendMail(): bool
    {
        return $this->email != '' && $this->email_notification;
    }

    /**
     * @return bool
     */
    public function isSendSms(): bool
    {
        return $this->phone != '' && $this->phone_notification;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool)$this->active;
    }

    /**
     * @return string|null
     */
    public function getPone(): ?string
    {
        return $this->phone;
    }
}
